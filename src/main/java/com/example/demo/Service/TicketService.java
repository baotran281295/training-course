package com.example.demo.Service;

import com.example.demo.Model.DateClose;
import com.example.demo.Model.Price;
import com.example.demo.Model.Result200dma;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

@Service
public class TicketService {
    private static final String API_KEY = "51QHx1xS-xJguGxrHQLK";

    public Price getTickerClosePrice (String tickerSymbol, String startDate, String endDate) {
        try {
            String tempURL = "https://www.quandl.com/api/v3/datasets/WIKI/" + tickerSymbol + "/data.json?api_key=" +
                    API_KEY + "&start_date=" + startDate + "&end_date=" + endDate;

            String jsonResponse = this.getJsonResponse(tempURL);

            return this.parseJson(jsonResponse, tickerSymbol);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public Result200dma get200dmaResult(String tickerSymbol, String startDate, int duration) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateFormat.parse(startDate));
        cal.add(Calendar.DATE, duration);
        String endDate = dateFormat.format(cal.getTime());
        try {
            String tempURL = "https://www.quandl.com/api/v3/datasets/WIKI/" + tickerSymbol + "/data.json?api_key=" +
                    API_KEY + "&start_date=" + startDate + "&end_date=" + endDate;

            String jsonResponse = this.getJsonResponse(tempURL);
            Price parsedPrice = this.parseJson(jsonResponse, tickerSymbol);
            if(parsedPrice == null) {
                return null;
            } else {
                Result200dma returnValue = new Result200dma();
                returnValue.setTicker(tickerSymbol);
                returnValue.setAvrerage(this.calculate200dmaValue(parsedPrice));
                return returnValue;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private String getJsonResponse(String tempURL) throws IOException {
        //create connection
        URL url = new URL(tempURL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");

        if (conn.getResponseCode() != 200) {
            return null;
        }

        BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

        //parse json response to string
        String jsonResponse = org.apache.commons.io.IOUtils.toString(br);

        conn.disconnect();

        return jsonResponse;
    }

    private Price parseJson (String jsonString, String tickerSymbol) {
        try {
            JSONObject obj = new JSONObject(jsonString);

            JSONObject datasetData = obj.getJSONObject("dataset_data");
            JSONArray data = datasetData.getJSONArray("data");

            ArrayList<DateClose> listDateClose = new ArrayList<>();
            for (int i = 0; i < data.length(); i++) {
                DateClose tempDateClose = new DateClose();

                //parse base on position of the key (date is 0th position and close price is 4th position)
                tempDateClose.setClosePrice(data.getJSONArray(i).get(4).toString());
                tempDateClose.setDate(data.getJSONArray(i).get(0).toString());
                listDateClose.add(tempDateClose);
            }

            Price returnPrice = new Price();
            returnPrice.setTicker(tickerSymbol);
            returnPrice.setDateCloses(listDateClose);

            return returnPrice;
        } catch (Exception ex) {
            return null;
        }
    }

    private float calculate200dmaValue(Price price)
    {
        ArrayList<DateClose> listDateClose = price.getDateCloses();
        float total = 0;

        //calculate total of days having value
        for(int i=0; i<listDateClose.size(); i++) {
            total += Float.parseFloat(listDateClose.get(i).getClosePrice());
        }
        return total/listDateClose.size();
    }
}
