package com.example.demo.Model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class Price {
    @JsonProperty("ticker")
    private String ticker;
    @JsonProperty("date_closes")
    private ArrayList<DateClose> dateCloses;

    public Price () {}

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public ArrayList<DateClose> getDateCloses() {
        return dateCloses;
    }

    public void setDateCloses(ArrayList<DateClose> dateCloses) {
        this.dateCloses = dateCloses;
    }
}
