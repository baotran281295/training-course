package com.example.demo.Model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Result200dma {
    @JsonProperty("Ticker")
    private String ticker;
    @JsonProperty("Avg")
    private float avrerage;

    public Result200dma() {}

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public float getAvrerage() {
        return avrerage;
    }

    public void setAvrerage(float avrerage) {
        this.avrerage = avrerage;
    }
}
