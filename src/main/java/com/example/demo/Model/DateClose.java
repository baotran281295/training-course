package com.example.demo.Model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DateClose {
    @JsonProperty("date")
    private String date;
    @JsonProperty("close_price")
    private String closePrice;

    public DateClose() {}

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getClosePrice() {
        return closePrice;
    }

    public void setClosePrice(String closePrice) {
        this.closePrice = closePrice;
    }
}
