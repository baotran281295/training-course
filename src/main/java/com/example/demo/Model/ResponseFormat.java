package com.example.demo.Model;

public class ResponseFormat {
    private int code;
    private String message;

    public ResponseFormat() {}

    public ResponseFormat(int code, String message)
    {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
