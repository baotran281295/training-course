package com.example.demo.RestController;

import com.example.demo.Model.Price;
import com.example.demo.Model.ResponseFormat;
import com.example.demo.Model.Result200dma;
import com.example.demo.Service.TicketService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping (value = "/api/v2")
public class TicketController {
    @Autowired
    private TicketService ticketService;

    @RequestMapping(value = "/{ticker_symbol}/closePrice", method = RequestMethod.GET)
    public ResponseEntity getClosePrice(@PathVariable(value = "ticker_symbol") String tickerSymbol,
                                        @RequestParam(value = "startDate", required = true) String startDate,
                                        @RequestParam(value = "endDate", required = true) String endDate)
                                    throws ParseException, JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        ResponseFormat validateValue = this.validateInput(tickerSymbol ,startDate, endDate);
        if(validateValue != null) {
            return  ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mapper.writeValueAsString(validateValue));
        }

        Price responsePrice = ticketService.getTickerClosePrice(tickerSymbol, startDate, endDate);
        HashMap<String, ArrayList<Price>> hm = checkAndFormatPriceData(responsePrice);
        if(hm == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    mapper.writeValueAsString(new ResponseFormat(404,"Cannot find the close price")));
        }

        return ResponseEntity.ok(mapper.writeValueAsString(hm));
    }

    @RequestMapping(value = "/{ticker_symbol}/200dma", method = RequestMethod.GET)
    public ResponseEntity<String> get200dmaClosePrice(@PathVariable(value = "ticker_symbol") String tickerSymbol,
                                                      @RequestParam(value = "startDate", required = true) String startDate)
                                            throws ParseException, JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        ResponseFormat validateValue = this.validateInput(tickerSymbol, startDate,null);
        if(validateValue != null) {
            return  ResponseEntity.status(HttpStatus.BAD_REQUEST).body(mapper.writeValueAsString(validateValue));
        }

        Result200dma responseData = ticketService.get200dmaResult(tickerSymbol, startDate, 199);
        if(responseData == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    mapper.writeValueAsString(new ResponseFormat(404, "Cannot find the result")));
        }
        HashMap<String, Result200dma> hm = new HashMap<>();
        hm.put("200dma", responseData);
        return ResponseEntity.ok(mapper.writeValueAsString(hm));
    }

    //this function is used to validate input
    private ResponseFormat validateInput(String tickerSymbol, String startDate, String endDate) throws ParseException {
        if(tickerSymbol.isEmpty()) {
            return new ResponseFormat(404, "Ticker symbol is empty");
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        if(dateFormat.parse(startDate).after(new Date())) {
            return new ResponseFormat(404, "Your start date have to be before or today");
        }

        if(endDate == null) {
            Date today = new Date();
            long diffInMillies = Math.abs(today.getTime() - dateFormat.parse(startDate).getTime());
            long duration = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
            if(duration < 200)
            {
                Calendar cal = Calendar.getInstance();
                cal.setTime(today);
                cal.add(Calendar.DATE, -199);
                Date suggestedTime = cal.getTime();
                String suggettedDate = dateFormat.format(suggestedTime);
                return new ResponseFormat(404,
                        "Your start date is not enough 200 days, it should be before " + suggettedDate);
            } else {
                return null;
            }
        }

        if(dateFormat.parse(endDate).after(new Date())) {
            return new ResponseFormat(404, "Your end date have to be before or today");
        }

        if(dateFormat.parse(endDate).before(dateFormat.parse(startDate))) {
            return new ResponseFormat(404, "Your start date have to be before end date");
        }

        return null;
    }

    private HashMap<String, ArrayList<Price>> checkAndFormatPriceData(Price price)
    {
        if(price==null) {
            return null;
        }

        //just make the response format correct
        ArrayList<Price> list = new ArrayList<>();
        list.add(price);

        HashMap<String,ArrayList<Price>> hm = new HashMap<>();
        hm.put("prices", list);

        return hm;
    }
}
